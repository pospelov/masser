var chat = {};

var INACTIVE = ImGuiWindowFlags.NoResize | ImGuiWindowFlags.NoMove | ImGuiWindowFlags.NoNav | ImGuiWindowFlags.NoInputs;

var F1 = 0x3B;
var F6 = F1 + 5;
var F8 = F1 + 7;

var ENTER = 0x1C;
var ESCAPE = 0x01;

var TRUE = 1;
var FALSE = 0;

var NULLVEC2 = vec2(0, 0);

var chatRenderTodo = [];
var emptyString = cstr('');
var windowCaption = cstr('Чат###Chat');
var scrollS = cstr('scroll');
var childSize = vec2(0, 0);
var x;
var y;
var cursor = false;
var nullptr = ptr(0);
var chatBufSize = 300;
var chatBuf = buf(chatBufSize);
var messages = [];
var setScrollHere = false;
var minSize = vec2(448, 317);
var chatOpen = true;
var badMenuOpen = false;
var channelSwitchVisible = true; // const
var numButtonLines = 0;

// chat channels
var currentChannel;
var channels = [];
var channelData = {};

function sendChatMessage(str) {
    if (str[0] == '/') {
        sendEvent('chatCommand', {
            channel: currentChannel,
            arguments: str.split(' ').slice(1).join(' '),
            cmd: str.split(' ').reverse().pop().split('').splice(1).join('') // without slash
        });
    } else {
        sendEvent('chatMessage', {
            message: str,
            channel: currentChannel
        });
    }
}

function setControlsEnabled(e) {
    var controls = ['Looking', 'Movement', 'Activate', 'CamSwitch'];
    for (var i = 0; i != controls.length; ++i) api.setControlEnabled(cstr(controls[i]), +e);
}

function changeFocus() {
    cursor = !cursor;
    api.setInputEnabled(cursor ? 0 : 1);
    setControlsEnabled(!cursor);
    api.gui_show_cursor(+cursor);
    chatRenderTodo.push(function() {
        cursor ? api.SetKeyboardFocusHere(0) : api.SetItemDefaultFocus();
    });
    if(cursor) chatOpen = true;
}

function getOptions() {
    var options = {};
    if (currentChannel && channelData[currentChannel]) {
        options = channelData[currentChannel].options;
        if (!options) options = {};
    }
    return options;
}

function getLightingColor(rgbBeginArg, rgbEndArg) {
    var rgbBegin = {
        r: 107, g: 163, b: 255
        //r: 255, g: 255, b: 255
    };
    var rgbEnd = {
        r: 53, g: 129, b: 255
    };
    if (rgbBeginArg) rgbBegin = rgbBeginArg;
    if (rgbEndArg) rgbEnd = rgbEndArg;

    var pi = 3.14;
    var sinVal = Math.sin(Date.now() / ((2000 / pi) / 2)); // ~ 2pi in 2 sec
    var rgbRes = {
        r: (rgbBegin.r + rgbEnd.r) / 2,
        g: (rgbBegin.g + rgbEnd.g) / 2,
        b: (rgbBegin.b + rgbEnd.b) / 2
    };
    var rgbDifference = {
        r: (rgbEnd.r - rgbBegin.r) / 2,
        g: (rgbEnd.g - rgbBegin.g) / 2,
        b: (rgbEnd.b - rgbBegin.b) / 2
    };
    rgbRes.r += Math.floor(rgbDifference.r * sinVal);
    rgbRes.g += Math.floor(rgbDifference.g * sinVal);
    rgbRes.b += Math.floor(rgbDifference.b * sinVal);

    return rgbRes.r.toString(16) + rgbRes.g.toString(16) + rgbRes.b.toString(16);
}

api.on('chatTick', function() {

    api.CustomSetAlpha(cursor ? 1.0 : 0.80);

    var badMenus = ["BarterMenu", "Book Menu", "Console", "Console Native UI Menu",
    "ContainerMenu", "Crafting Menu", "FavoritesMenu", "GiftMenu", "InventoryMenu",
    "Journal Menu", "LevelUp Menu", "Lockpicking Menu", "MagicMenu",  "MapMenu",
    "MessageBoxMenu", "RaceSex Menu", "Sleep / Wait Menu", "StatsMenu", "Training Menu",
    "TweenMenu"];
    for (var i = 0; i != badMenus.length; ++i) {
        if (api.isMenuOpen(cstr(badMenus[i]))) {
            badMenuOpen = true;
            return;
        }
    }
    badMenuOpen = false;
});

api.on('render', function() {
    if (Math.random() < 0.25) api.chatTick();

    var flags = cursor ? ImGuiWindowFlags.ResizeFromAnySide : INACTIVE;
    flags |= ImGuiWindowFlags.NoCollapse;

    if (!chatOpen || badMenuOpen) return;

    api.PushStyleColorU32(ImGuiCol.SeparatorActive, toabgr(getLightingColor()));
    api.Begin(windowCaption, nullptr, flags); {
        api.SetWindowSize(minSize, ImGuiCond.FirstUseEver);

        var currentSizePtr = vec2();
        api.GetWindowSize(currentSizePtr);
        var xy = getVec2(currentSizePtr);

        numButtonLines = 0;
        if (channelSwitchVisible) {
            numButtonLines = 1;

            var buttonsTotalWidth = 0;
            for (var i = 0; i != channels.length; ++i) {
                var ch = channels[i];
                var name = channelData[ch].name;
                var pressed;

                var btnSize = vec2(15 * name.length, 25);
                buttonsTotalWidth += 15 * name.length + 7;

                var buttonsTotalWidthNext = buttonsTotalWidth;
                if (i < channels.length - 1) {
                    buttonsTotalWidthNext += channelData[channels[i + 1]].name.length * 15;
                }

                api.PushStyleColorU32(ImGuiCol.NavHighlight, toabgr(getLightingColor()));
                if (!channelData[ch].blink) { // нет мерцания, просто рисуем кнопку
                    pressed = api.Button(cstr(name), btnSize);
                } else { // надо нарисовать с другим цветом текста
                    api.PushStyleColorU32(ImGuiCol.Text, toabgr(getLightingColor({r: 255, g: 255, b: 255})));
                    pressed = api.Button(cstr(name), btnSize);
                    api.PopStyleColor(1);
                }
                api.PopStyleColor(1);

                if (i + 1 != channels.length) {
                    if (buttonsTotalWidthNext < xy[0]) api.SameLine(0, -1);
                    else {
                        buttonsTotalWidth = 0;
                        ++numButtonLines;
                    }
                }
                if (pressed) {
                    chat.setCurrentChannel(ch);
                }
            }
        }

        if (!x || Math.random() < 1.0) { // 0.1
            x = api.igGetWindowWidth() - 10;
            y = api.igGetWindowHeight() - 75 - ((25 + 7) * numButtonLines);
            if (getOptions().noInput) y += 25;
            setVec2(childSize, x, y);
        }
        api.BeginChild(scrollS, childSize, TRUE, 0); {
            if (true) {
                var msgs = currentChannel ? channelData[currentChannel].messages : messages;
                for (var i = 0; msgs && i != msgs.length; ++i) {
                    api.PushStyleColorU32(ImGuiCol.Text, msgs[i].color);
                    api.TextWrapped(msgs[i].cstr);
                    api.PopStyleColor(1);
                }
            }
            api.Text(emptyString);
            if (setScrollHere) {
                setScrollHere = false;
                api.SetScrollHere(1.0);
            }
        } api.EndChild();

        if (!getOptions().noInput) {

            var txt = getLightingColor();
            ///api.Text(cstr(txt));
            api.PushStyleColorU32(ImGuiCol.NavHighlight, toabgr(txt));
            if (!cursor) api.PushStyleColorU32(ImGuiCol.Text, 0x00000000);
            api.InputText(emptyString, chatBuf, chatBufSize, cursor ? 0 : ImGuiInputTextFlags.ReadOnly, nullptr, nullptr);
            if (!cursor) api.PopStyleColor(1);
            api.PopStyleColor(1);
        }

        if (Math.random() < 0.2 && chatRenderTodo.length != 0) {
            chatRenderTodo.forEach(function(el) { el(); });
            chatRenderTodo = [];
        }

    } api.End();
    api.PopStyleColor(1);
});

api.on('mouse', function(dxKeyCode, eventName, x, y) {

});


var lastRelease = {};

api.on('keyboard', function(dxKeyCode, eventName) {

    var ms = lastRelease[dxKeyCode] != undefined ? Date.now() - lastRelease[dxKeyCode] : Infinity;
    if(eventName == 'release') lastRelease[dxKeyCode] = Date.now();

    if (eventName == 'release' && dxKeyCode == F6 || (dxKeyCode == ESCAPE && cursor)) {
        if (ms > 133) {
            var timeout = dxKeyCode == ESCAPE ? 67 : 1;
            setTimeout(function() {
                changeFocus();
            }, timeout);
        }
    }
    if (eventName == 'release' && dxKeyCode == F8) {
        if (ms > 133) chatOpen = !chatOpen;
    }
    if(eventName == 'release' && dxKeyCode == ENTER) {
        if (cursor) {
            var chatStr = Memory.readUtf8String(chatBuf);
            if (chatStr != '') {
                sendChatMessage(chatStr);
            }
            Memory.writeU8(chatBuf, 0);
            chatRenderTodo.push(function() {
                api.SetKeyboardFocusHere(0);
            });
        }
    }
});

chat.createChannel = function(channel, name, priority, options) {
    channels.push(channel);
    channelData[channel] = {
        name: name,
        messages: [],
        priority: priority,
        options: options
    };
    channels.sort(function(a, b) {
        return channelData[b].priority - channelData[a].priority;
    });
}

chat.setCurrentChannel = function(ch) {
    currentChannel = ch;
    chat.currentChannel = ch;
    windowCaption = cstr(channelData[ch].name + '###Chat');
    if (api.chatSetCurrentChannel) api.chatSetCurrentChannel(ch);
}

chat.destroyChannel = function(channel) {
    for (var i = 0; i != channels.length; ++i) {
        if (channels[i] == channel) {
            channels.splice(i, 1);
            break;
        }
    }
    if (currentChannel == channel) {
        currentChannel = channels.length ? channels[0] : undefined;
        chat.currentChannel = channels.length ? channels[0] : undefined;
    }
}

chat.addMessage = function(channel, str, color) {
    setScrollHere = true;
    if (!color) color = 'FFFFFF';
    if (!channel) {
        messages.push({
            cstr: cstr(str),
            color: toabgr(color)
        });
    } else {
        if (!channelData[channel].messages) {
            channelData[channel].messages = [];
        }
        channelData[channel].messages.push({
            cstr: cstr(str),
            color: toabgr(color)
        });
    }
}

chat.setBlink = function(channel, blink) {
    if (chat.currentChannel != channel) {
        channelData[channel].blink = blink;
    }
}
