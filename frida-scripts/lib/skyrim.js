var skyrim = {};

skyrim.queue = [];

skyrim.printNote = function(str) {
    skyrim.queue.push(function() {
        api.Notification(cstr(str));
    });
};

api.on('update', function() {
    if (skyrim.queue.length != 0) {
        skyrim.queue.forEach(function(f) {
            f();
        });
        skyrim.queue = [];
    }
});
