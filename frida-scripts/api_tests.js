// parseFnDecl
var tests = [
    'bool(bool *, char *)',
    'string()',
    'void(cstr, cstr, ...)'
];
tests = tests.map(function(el) { return parseFnDecl(el); });
var v1;
var v2;
v1 = JSON.stringify(tests[0]);
v2 = JSON.stringify({
    returns: 'uint8',
    args: ['pointer', 'pointer']
});
if (v1 != v2) {
    throw v1 + ' != ' + v2;
}
v1 = JSON.stringify(tests[1]);
v2 = JSON.stringify({
    returns: 'pointer',
    args: []
});
if (v1 != v2) {
    throw v1 + ' != ' + v2;
}
v1 = JSON.stringify(tests[2]);
v2 = JSON.stringify({
    returns: 'void',
    args: ['pointer', 'pointer', '...']
});
if (v1 != v2) {
    throw v1 + ' != ' + v2;
}
tests = undefined;
v1 = undefined;
v2 = undefined;
