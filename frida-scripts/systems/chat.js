
var MIRROR_CHANNEL = '_TheMirrorChannel';
var MIRROR_NAME = 'Чат';

var hasMirror = false;
var channelsToMirror = {};

api.on('channelInvite', function(data) {
    if (data.mirror) {
        ///throw 'mirror';
        if (!hasMirror) {
            chat.createChannel(MIRROR_CHANNEL, MIRROR_NAME, 999999, { noInput: true});
            hasMirror = true;
        }
        channelsToMirror[data.channel] = true;
    }
    chat.createChannel(data.channel, data.name, data.priority);
    if (data.focus) chat.setCurrentChannel(data.channel);
});

api.on('channelUninvite', function(data) {
    chat.destroyChannel(data.channel);
    channelsToMirror[data.channel] = false;
});

api.on('channelMessage', function(data) {
    chat.addMessage(data.channel, data.message, data.color);

    if (chat.currentChannel != MIRROR_CHANNEL && data.channel != chat.currentChannel) {
        chat.setBlink(data.channel, true);
    }

    if (channelsToMirror[data.channel]) {
        chat.addMessage(MIRROR_CHANNEL, data.message, data.color);
    }
});

api.on('chatSetCurrentChannel', function(channel) {
    if (channel == MIRROR_CHANNEL) {
        for (ch in channelsToMirror) {
            chat.setBlink(ch, false);
        }
    }
})

api.on('packet', function(packetName) {
    if (packetName == 'ID_WRONG_PASS') {
        chat.addMessage(null, 'Неправильный серверный пароль');
    }
    if (packetName == 'ID_NAME_INVALID') {
        chat.addMessage(null, 'Некорректное имя персонажа');
    }
    if (packetName == 'ID_NAME_ALREADY_USED') {
        chat.addMessage(null, 'Имя персонажа уже используется одним из игроков на сервере');
    }
    if (packetName == 'ID_WELCOME') {
        chat.addMessage(null, 'Соединение установлено');
    }
    if (packetName == 'ID_SERVER_CLOSED_THE_CONNECTION') {
        chat.addMessage(null, 'Сервер закрыл соединение');
    }
});

chat.addMessage(null, 'Соединение с серверами SkyMP...');
