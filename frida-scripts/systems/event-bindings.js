
api.on('printNote', function(data) {
    skyrim.printNote(data.message);
});

// demo

var editorEnabled = false;

api.on('render', function() {
    if (editorEnabled) {
        api.Begin(cstr('Editor'), ptr(0), 0); {
            api.ShowStyleEditor(api.GetStyle());
        } api.End();
    }
});

api.on('keyboard', function(code, eventname) {
    if (code == 60 /*f2*/ && eventname == 'release') {
        editorEnabled = !editorEnabled;
    }
})
