'use strict';

var __vecStorage = {};
var __strStorage = {};
var __totalRamUsed = 0;

function getMemoryUse() {
    return __totalRamUsed;
}

function getVec2(vecPtr) {
    return [Memory.readFloat(vecPtr), Memory.readFloat(ptr(4 + vecPtr.toInt32()))];
}

function vec2(x, y) {
    if (x === undefined && y === undefined) {
        var vecPtr = Memory.alloc(8);
        setVec2(vecPtr, 0, 0);
        return vecPtr;
    }
    var key = [x, y].join(';');
    if (!__vecStorage[key]) {
        var vecPtr = Memory.alloc(8);
        setVec2(vecPtr, x, y);
        __vecStorage[key] = vecPtr;
        __totalRamUsed += 8;
    }
    return __vecStorage[key];
}

function setVec2(vecPtr, x, y) {
    Memory.writeFloat(vecPtr, x);
    Memory.writeFloat(ptr(vecPtr.toInt32() + 4), y);
}

function cstr(string) {
    if (typeof string == 'object') {
        string = JSON.stringify(string, null, 2);
    } else if (typeof string == 'array') {
        string = '[' + string.join(', ') + ']';
    }
    string = '' + string;
    if (__strStorage[string] === undefined) {
        __strStorage[string] = Memory.allocUtf8String(string);
        __totalRamUsed += string.length;
    }
    return __strStorage[string];
}

function buf(len) {
    var res = Memory.alloc(len)
    Memory.writeU8(res, 0);
    return res;
}

// TODO: Аллоцировать память большими кусками заранее, а не по 1 байту каждый раз
function newBool(value) {
    var ptr = Memory.alloc(1);
    setBool(ptr, value);
    return ptr;
}

function setBool(ptr, value) {
    Memory.writeU8(ptr, value ? 1 : 0);
}

function getBool(ptr, value) {
    return Memory.readU8(ptr) != 0;
}

function toabgr(str) {
    return parseInt('0xFF' + ('' + str).match(/.{1,2}/g).reverse().join(''));
}
