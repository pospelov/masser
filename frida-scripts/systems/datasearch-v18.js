
var updated = false;
var dsEnabled = false;
var dataHolder = {}; // data by refID

api.on('update', function() {
    updated = true;
});

api.on('enableDataSearch', function(data) {
    dsEnabled = !!data.enable;
    ///chat.addMessage(chat.currentChannel, 'ds enabled = ' + dsEnabled, 'ffffff');
});

function sendDSData(data) {
    // TODO: Посмотреть на это и подумать..
    var ms = Math.floor(Math.random() * 2000);
    setTimeout(function() {
        sendEvent('dataSearchResult', {
            data: data
        });
    }, ms);
}

function getLocationID(form) {
    var cell = api.GetParentCell(form);
    if (cell.toInt32() != 0 && !api.IsInterior(cell)) cell = ptr(0);
    var res = cell.toInt32() != 0 ? cell : api.GetWorldSpace(form);
    return api.GetFormID(res);
}

function notMinus1(x) {
    return x != -1;
}

api.on('dealWithRef', function(formID, type) {
//return;
    if (!updated || !dsEnabled) return;
    if (isNaN(formID)) return;
    if (dataHolder[formID]) return;

    var form = api.GetFormById(formID);
    if (api.IsDisabled(form)) return;

    //skyrim.printNote('formid: ' + formID + ' form: ' + form.toString())

//api.GetBaseObject(form)
    //return;
    //skyrim.printNote(ptr.toInt32() + ' ' + type);

    var base = api.GetBaseObject(form);
    var data = {
        refID: api.GetFormID(form),
        baseID: api.GetFormID(base),
        pos: {
            x: api.GetPositionX(form),
            y: api.GetPositionY(form),
            z: api.GetPositionZ(form)
        },
        rot: {
            x: api.GetAngleX(form),
            y: api.GetAngleY(form),
            z: api.GetAngleZ(form)
        },
        location: getLocationID(form),
        type: 'Static'
    };

    const CONT = 28;
    const DOOR = 29;
    const ACTI = 24;
    const FURN = 40;

    // Items:
    const MISC = 32;
    const WEAP = 41;
    const ARMO = 26;
    const INGR = 30;
    const ALCH = 46; // Potion
    const SLGM = 52;
    const AMMO = 42;
    const BOOK = 27;
    const KEY = 45;

    switch (type) {
        case CONT: {
            //skyrim.printNote('hey container');
            var i = 0;
            var item = api.GetNthItem(form, i);
            var items = [];
            while (!item.isNull()) {
                i++;
                item = api.GetNthItem(form, i);
                items.push(item);
            }

            var itemIDs = items.map(function(x) { return (x && x.isNull() == false) ? api.GetFormID(x) : -1; }).filter(notMinus1);

            var inventory = {};
            itemIDs.forEach(function(id) {
                var count = api.GetItemCount(form, api.GetFormById(id));
                if (count > 0) inventory['' + id] = count;
            });

            data.inventory = inventory;
            data.type = 'Container';
            break;
        }
        case DOOR: {
            data.type = 'Door';
            break;
        }
        case ACTI: {
            data.type = 'Activator';
            break;
        }
        case FURN: {
            data.type = 'Furniture';
            break;
        }
        case MISC:
        case WEAP:
        case ARMO:
        case INGR:
        case ALCH:
        case SLGM:
        case AMMO:
        case BOOK:
        case KEY: {
            data.type = 'Item';
            break;
        }
    }

    ///chat.addMessage(chat.currentChannel, JSON.stringify(data), '33ccff')

    dataHolder[data.refID] = data;
    if (data.type != 'Static') {

        // Откл предметы в домах
        /*if (api.ObjectReference_GetActorOwner(form) || api.ObjectReference_GetFactionOwner(form)) {
            try {
                var formid = null;
                if (api.ObjectReference_GetFactionOwner(form).isNull() == false) {
                    formid = api.GetFormID(api.ObjectReference_GetFactionOwner(form));
                }
                if (data.type == 'Item' && 0x3c != api.GetFormID(api.GetParentCell(api.GetPlayer()))) return;
            }
            catch(e) {
            }
        }*/

        sendDSData(data);
    }
});
