var api = {};

api.on = function(str, fn) {
    if (typeof api[str] == 'function') {
        var fnWas = api[str];
        api[str] = function(a1, a2, a3, a4, a5, a6, a7, a8) {
            fnWas(a1, a2, a3, a4, a5, a6, a7, a8);
            fn(a1, a2, a3, a4, a5, a6, a7, a8);
        };
    } else {
        api[str] = fn;
    }
}

function parseFnDecl(decl) {
    var t = function(type) {
        if (type == 'bool') {
            return 'uint8';
        }
        if (type == 'function' || type == 'string' || type == 'cstr' || type == 'vec2') {
            return 'pointer';
        }
        if (type.split('').some(function(c){ return c == '*';})) {
            return 'pointer';
        }
        if(type == 'flags') {
            return 'int32';
        }
        return type;
    };
    var arr = decl.match(/[^,()]+/g).map(function(x) { return t(x.trim()); });
    var args = [];
    for (var i = 1; i != arr.length; ++i) {
        args.push(arr[i]);
    }
    return {
        returns: arr[0],
        args: args
    };
}

function createNativeFunctions(functions) {
    for (var name in functions) {
        var decl = functions[name];
        var fnData = parseFnDecl(decl);

        var ptr_ = Module.findExportByName(null, name);
        if (ptr_ == null) {
            throw name + ': кто-то забыл написать __declspec(dllexport) в объявлении';
        }
        var nativeFn = new NativeFunction(ptr_, fnData.returns, fnData.args);
        api[name] = nativeFn;

        // igText -> Text
        if (name[0] == 'i' && name[1] == 'g') {
            api[name.slice(2)] = nativeFn;
        }
    }
}

createNativeFunctions({
    igButton: 'bool(string, vec2)',
    igText: 'void(string, ...)',
    igTextWrapped: 'void(string, ...)',
    igBegin: 'bool(string, bool*, int)',
    igEnd: 'void()',
    igInputText: 'bool(string, char *, int, flags, function, void *)',
    igSetKeyboardFocusHere: 'void(int)',
    igSetItemDefaultFocus: 'void()',
    igBeginChild: 'bool(string, vec2, bool, flags)',
    igEndChild: 'void()',
    igGetWindowWidth: 'float()',
    igGetWindowHeight: 'float()',
    igGetScrollX: 'float()',
    igGetScrollY: 'float()',
    igGetScrollMaxX: 'float()',
    igGetScrollMaxY: 'float()',
    igSetScrollX: 'void(float)',
    igSetScrollY: 'void(float)',
    igSetScrollHere: 'void(float)',
    igSetWindowSize: 'void(vec2, flags)',
    igGetWindowSize: 'void(vec2_out *)',
    igSetWindowFocus: 'void(pointer)',
    igSameLine: 'void(float, float)',
    igPushStyleColorU32: 'void(int, uint32)',
    igPopStyleColor: 'void(int)',
    igColumns: 'void(int, char*, bool)', // wtf
    igSeparator: 'void()', // wtf
    igNextColumn: 'void()', // wtf
    igSelectable: 'bool(string, bool, flags, vec2)', // wtf
    igListBox: 'bool(char *, int *, char **, int, int)',
    igCustomSetAlpha: 'void(float)',
    igGetStyle: 'pointer()',
    igShowStyleEditor: 'void(pointer)',


    GetKeyPressed: 'bool(uint8)',
    gui_show_cursor: 'void(bool)',
    sendEvent: 'void(string, string),',
    setControlEnabled: 'void(string, bool)',
    setInputEnabled: 'void(bool)',
    isMenuOpen: 'bool(string)',

    GetPlayer: 'pointer()',
    Notification: 'void(pointer)',
    GetFormID: 'uint(TESForm *)',
    GetBaseObject: 'pointer(TESObjectREFR *)',
    GetPositionX: 'float(TESObjectREFR *)',
    GetPositionY: 'float(TESObjectREFR *)',
    GetPositionZ: 'float(TESObjectREFR *)',
    GetAngleX: 'float(TESObjectREFR *)',
    GetAngleY: 'float(TESObjectREFR *)',
    GetAngleZ: 'float(TESObjectREFR *)',
    GetParentCell: 'pointer(TESObjectREFR *)',
    GetWorldSpace: 'pointer(TESObjectREFR *)',
    IsInterior: 'bool(TESObjectCELL *)',
    GetFormById: 'pointer(uint)',
    GetNthItem: 'pointer(pointer, int)',
    GetItemCount: 'uint(TESObjectREFR *, TESForm *)',
    IsDisabled: 'bool(TESObjectREFR *)',
    ObjectReference_GetActorOwner: 'pointer(TESObjectREFR *)',
    ObjectReference_GetFactionOwner: 'pointer(TESObjectREFR *)',
    SetDestroyed: 'void(TESObjectREFR *, bool)'
});

function sendEvent(evnName, evnData) {
    api.sendEvent(cstr(evnName), cstr(JSON.stringify(evnData)));
}


Interceptor.attach(Module.findExportByName(null, 'skymp_render_hook'), {
    onEnter: function (args) {
        if (api.render != undefined) {
            api.render();
        }
    },
    onLeave: function (retVal) {
    }
});

Interceptor.attach(Module.findExportByName(null, 'skymp_mouse_hook'), {
    onEnter: function (args) {
        if (api.mouse != undefined) {
            this.code = args[0].toInt32();
            this.eventName = Memory.readCString(args[1]);
            this.x = args[2].toInt32();
            this.y = args[3].toInt32();
            api.mouse(this.code, this.eventName, this.x, this.y);
        }
    },
    onLeave: function (retVal) {
    }
});
Interceptor.attach(Module.findExportByName(null, 'skymp_keyboard_hook'), {
    onEnter: function (args) {
        if (api.keyboard != undefined) {
            this.code = args[0].toInt32();
            this.eventName = Memory.readCString(args[1]);
            api.keyboard(this.code, this.eventName);
        }
    },
    onLeave: function (retVal) {
    }
});
Interceptor.attach(Module.findExportByName(null, 'skymp_packet_hook'), {
    onEnter: function (args) {
        if (api.packet != undefined) {
            this.name = Memory.readCString(args[0]);
            this.data = ptr(args[1].toInt32());
            this.dataLen = args[2].toInt32();
            api.packet(this.name, this.data, this.dataLen);
        }
    },
    onLeave: function (retVal) {
    }
});
Interceptor.attach(Module.findExportByName(null, 'skymp_event_hook'), {
    onEnter: function (args) {
        this.data = Memory.readCString(args[0]);
        var evn = JSON.parse(this.data);
        var evnName = evn.name;
        var evnData = evn.data;
        if (api[evn.name]) api[evn.name](evn.data);
    },
    onLeave: function (retVal) {
    }
});
Interceptor.attach(Module.findExportByName(null, 'skymp_update_hook'), {
    onEnter: function (args) {
        if (api.update) api.update();
    },
    onLeave: function (retVal) {
    }
});
Interceptor.attach(Module.findExportByName(null, 'skymp_dealwithref_hook'), {
    onEnter: function (args) {
        var refID = parseInt(Memory.readCString(args[0]));
        var type = args[1].toInt32();
        if (api.dealWithRef) api.dealWithRef(refID, type);
    },
    onLeave: function (retVal) {
    }
});

var ImGuiWindowFlags = {
    NoTitleBar             : 1 << 0,   // Disable title-bar
    NoResize               : 2,   // Disable user resizing with the lower-right grip
    NoMove                 : 4,   // Disable user moving the window
    NoScrollbar            : 1 << 3,   // Disable scrollbars (window can still scroll with mouse or programatically)
    NoScrollWithMouse      : 1 << 4,   // Disable user vertically scrolling with mouse wheel. On child window, mouse wheel will be forwarded to the parent unless NoScrollbar is also set.
    NoCollapse             : 1 << 5,   // Disable user collapsing window by double-clicking on it
    AlwaysAutoResize       : 1 << 6,   // Resize every window to its content every frame
    //ImGuiWindowFlags_ShowBorders          = 1 << 7,   // Show borders around windows and items (OBSOLETE! Use e.g. style.FrameBorderSize=1.0f to enable borders).
    NoSavedSettings        : 1 << 8,   // Never load/save settings in .ini file
    NoInputs               : 1 << 9,   // Disable catching mouse or keyboard inputs, hovering test with pass through.
    MenuBar                : 1 << 10,  // Has a menu-bar
    HorizontalScrollbar    : 1 << 11,  // Allow horizontal scrollbar to appear (off by default). You may use SetNextWindowContentSize(ImVec2(width,0.0f)); prior to calling Begin() to specify width. Read code in imgui_demo in the "Horizontal Scrolling" section.
    NoFocusOnAppearing     : 1 << 12,  // Disable taking focus when transitioning from hidden to visible state
    NoBringToFrontOnFocus  : 1 << 13,  // Disable bringing window to front when taking focus (e.g. clicking on it or programatically giving it focus)
    AlwaysVerticalScrollbar: 1 << 14,  // Always show vertical scrollbar (even if ContentSize.y < Size.y)
    AlwaysHorizontalScrollbar:1<< 15,  // Always show horizontal scrollbar (even if ContentSize.x < Size.x)
    AlwaysUseWindowPadding : 1 << 16,  // Ensure child windows without border uses style.WindowPadding (ignored by default for non-bordered child windows, because more convenient)
    ResizeFromAnySide      : 1 << 17,  // [BETA] Enable resize from any corners and borders. Your back-end needs to honor the different values of io.MouseCursor set by imgui.
    NoNavInputs            : 1 << 18,  // No gamepad/keyboard navigation within the window
    NoNavFocus             : 1 << 19,  // No focusing toward this window with gamepad/keyboard navigation (e.g. skipped by CTRL+TAB)
    NoNav                  : (1 << 18) | (1 << 19)
}

var ImGuiInputTextFlags = {
    CharsDecimal        : 1 << 0,   // Allow 0123456789.+-*/
    CharsHexadecimal    : 1 << 1,   // Allow 0123456789ABCDEFabcdef
    CharsUppercase      : 1 << 2,   // Turn a..z into A..Z
    CharsNoBlank        : 1 << 3,   // Filter out spaces, tabs
    AutoSelectAll       : 1 << 4,   // Select entire text when first taking mouse focus
    EnterReturnsTrue    : 1 << 5,   // Return 'true' when Enter is pressed (as opposed to when the value was modified)
    CallbackCompletion  : 1 << 6,   // Call user function on pressing TAB (for completion handling)
    CallbackHistory     : 1 << 7,   // Call user function on pressing Up/Down arrows (for history handling)
    CallbackAlways      : 1 << 8,   // Call user function every time. User code may query cursor position, modify text buffer.
    CallbackCharFilter  : 1 << 9,   // Call user function to filter character. Modify data->EventChar to replace/filter input, or return 1 to discard character.
    AllowTabInput       : 1 << 10,  // Pressing TAB input a '\t' character into the text field
    CtrlEnterForNewLine : 1 << 11,  // In multi-line mode, unfocus with Enter, add new line with Ctrl+Enter (default is opposite: unfocus with Ctrl+Enter, add line with Enter).
    NoHorizontalScroll  : 1 << 12,  // Disable following the cursor horizontally
    AlwaysInsertMode    : 1 << 13,  // Insert mode
    ReadOnly            : 1 << 14,  // Read-only mode
    Password            : 1 << 15,  // Password mode, display all characters as '*'
    NoUndoRedo          : 1 << 16,  // Disable undo/redo. Note that input text owns the text data while active, if you want to provide your own undo/redo stack you need e.g. to call ClearActiveID().
    CharsScientific     : 1 << 17  // Allow 0123456789.+-*/eE (Scientific notation input)
}

var ImGuiCond = {
	Always: 1 << 0,
	Once: 1 << 1,
	FirstUseEver: 1 << 2,
	Appearing:1 << 3
}

var ImGuiCol = {
    Text: 0,
    TextDisabled: 1,
    WindowBg: 2,              // Background of normal windows
    ChildBg: 3,               // Background of child windows
    PopupBg: 4,               // Background of popups, menus, tooltips windows
    Border: 5,
    BorderShadow: 6,
    FrameBg: 7,               // Background of checkbox, radio button, plot, slider, text input
    FrameBgHovered: 8,
    FrameBgActive: 9,
    TitleBg: 10,
    TitleBgActive: 11,
    TitleBgCollapsed: 12,
    MenuBarBg: 13,
    ScrollbarBg: 14,
    ScrollbarGrab: 15,
    ScrollbarGrabHovered: 16,
    ScrollbarGrabActive: 17,
    CheckMark: 18,
    SliderGrab: 19,
    SliderGrabActive: 20,
    Button: 21,
    ButtonHovered: 22,
    ButtonActive: 23,
    Header: 24,
    HeaderHovered: 25,
    HeaderActive: 26,
    Separator: 27,
    SeparatorHovered: 28,
    SeparatorActive: 29,
    ResizeGrip: 30,
    ResizeGripHovered: 31,
    ResizeGripActive: 32,
    PlotLines: 33,
    PlotLinesHovered: 34,
    PlotHistogram: 35,
    PlotHistogramHovered: 36,
    TextSelectedBg:37,
    ModalWindowDarkening: 38,  // darken/colorize entire screen behind a modal window, when one is active
    DragDropTarget: 39,
    NavHighlight: 40,          // gamepad/keyboard: current highlighted item
    NavWindowingHighlight: 41, // gamepad/keyboard: when holding NavMenu to focus/move/resize windows
};
