var dialog = {};

dialog.tasks = [];

api.on('render', function() {
    dialog.tasks.forEach(function(f) {
        //try { f(); } catch(e) { throw a };
        f();
    });
});

dialog.showList = function(caption, listItems, buttons, description) {
    if (!description) description = '';

    var currentItem = Memory.alloc(4);
    var items = Memory.alloc(4 * listItems.length);
    for(var k = 0; k != listItems.length; ++k) {
        Memory.writeU32(ptr(items.toInt32() + k * 4), cstr(listItems[k]).toInt32());
    }

    var renderer = function() {

        if (!cursor) return;

        var flags = ImGuiWindowFlags.AlwaysAutoResize | ImGuiWindowFlags.NoCollapse;

        api.Begin(cstr(caption), ptr(0), flags);
        api.ListBox(cstr(description), currentItem, items, listItems.length, listItems.length + 5);
        api.End();
    };
    dialog.tasks.push(renderer);
};
